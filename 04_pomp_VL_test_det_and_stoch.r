# Script to test deterministic and stochastic model for transmission and
# detection/treatment of VL for the NTDmc-VL paper in JID 2019

# Key assumptions:
#  - fixed population size
#  - no age structure
#  - deterministic or stochastic human population


# Author: Luc Coffeng, Johanna Munoz
# Created: 9 July 2019

# Prep
  rm(list = ls())
  
  library(pomp2)
  library(ggplot2)
  library(data.table)
  library(boot)

  library(rstudioapi)  # requires RStudio
  
  code.dir <- dirname(getActiveDocumentContext()$path)
  
  setwd(file.path(code.dir, "01_Source"))
  source("pomp_VL_model.r")
  source("pomp_VL_param_values.r")
  source("pomp_VL_helper_functions.r")
  
  
# Set simulation duration, stepsize (in years) and time-dependent variables
# interpolated over time)
  start.time <- 0
  sim.duration <- 200
  dt.output.det <- 1 / 365  # set to daily to be able to calculate annual average
  dt.output.stoch <- 1      # will produce cumulative incidence/mortality per year
  policy <- covariate_table(rhoI1 = c(1/192, 1/65, 1/192, 1/192) * 365,
                            rhoI2 = rep(1/192, 4) * 365,
                            fd = rep(1, 4),
                            order = "constant",
                            times = c(-1, 100, 105, 1e4))  
  
  
  param.values.policy <- param.values[!(names(param.values) %in%
                                          c("rhoI1", "rhoI2", "fd"))] 
  
  
# Compile deterministic models
  time.seq.det <- seq(start.time,
                      start.time + sim.duration,
                      by = dt.output.det)
  
  model.det <- pomp(data = data.frame(dummy = NA,
                                      time = time.seq.det),
                    times = "time",
                    covar = policy,
                    t0 = 0,
                    skeleton = vectorfield(Csnippet(vl.code.det)),
                    rinit = Csnippet(vl.det.init),
                    statenames = statenames.det,
                    paramnames = names(c(param.values.policy, init.values)))
  

# Run deterministic models
  output.det <- trajectory(model.det,
                           params = c(param.values.policy,
                                      init.values))
  
  ### Next call will crash because parameters rhoI1 and rhoI2 are no longer in
  ### the parameter vector but are defined in the covariate table. To be fixed:
  ### the function extract.output.det() now has an additional argument "covars",
  ### but its implementation inside the function still needs to happen.
  output.det <- extract.output.det(x = output.det,
                                   time = time.seq.det,
                                   pars = param.values.policy,
                                   covars = policy)

    
# Run stochastic model (compiling happens withint run.stoch() function)
  n.sim <- 25
  stoch.pop.size <- 1e4  # must result in integers when multiplied with init.values

  t.start <- 105
  init.values.stoch.new <-
    rmultinom(n = n.sim,
              size = stoch.pop.size,
              prob = unlist(output.det[time == t.start,
                                       mget(statenames.det)]))
  rownames(init.values.stoch.new) <- paste0(rownames(init.values.stoch.new), "_0")
  
  output.stoch <- run.stoch(time.output = seq(t.start,
                                              t.start + (sim.duration - start.time - t.start),
                                              by = dt.output.stoch),
                            time.step =  1 / (2*365),
                            t0 = t.start,
                            pars = param.values.policy,
                            init = init.values.stoch.new,
                            covar = policy,
                            seed = 123456,
                            nsim = 1)  # will run one simulation for every set of initial values
  output.stoch[, sim := factor(sim)]
  
  
# Prep deteministic model output for plotting
  output.det.annual <- copy(output.det[time != max(time)])  # drop the last record(s) because it cannot be used to calculate the average for that year
  output.det.annual[, time := floor(time) + 1]  # plus one to match timing of reporting as in stochastic model (i.e. reporting over the previous period!)
  output.det.annual <- output.det.annual[, .( VL_I11 = sum(VL_I11) / .N,
                                              VL_I12 = sum(VL_I12) / .N,
                                              VL_I13 = sum(VL_I13) / .N,
                                              VL_I21 = sum(VL_I21) / .N,
                                              VL_I22 = sum(VL_I22) / .N,
                                              VL_I23 = sum(VL_I23) / .N,
                                              VL_inc = sum(VL_inc) / .N,
                                              VL_inc_obs = sum(VL_inc_obs) / .N,
                                              VL_death_h = sum(VL_death_1) / .N,
                                              VL_death_l = sum(VL_death_2) / .N),
                                         by = time]
  
  output.stoch[time == t.start,
               VL_inc_obs := output.det.annual[time == t.start, VL_inc_obs * stoch.pop.size]]
  
  
# Quick check of proportion of VL cases that are detected and treated before
# they die
  round(output.det.annual[time %in% 98:105,
                          .(time,
                            VL_inc = VL_inc*1e4,
                            VL_inc_obs = VL_inc_obs*1e4,
                            prop_detected_approx = VL_inc_obs / VL_inc)], 2)
  
  
#############
### Plots ###
#############
  
# Compare deterministic and stochastic model predictions
  compare.plot <- ggplot(data = output.stoch,
                         mapping = aes(x = time - 100, y = VL_inc_obs / N * 1e4)) +
    geom_line(mapping = aes(group = sim, col = "Stochastic (individual simulations)"),
              alpha = 0.1) +
    stat_summary(fun.y = mean, geom = "line",
                 mapping = aes(col = "Stochastic (mean of repeated simulations)")) +
    geom_line(data = output.det.annual,
              mapping = aes(y = VL_inc_obs * 1e4,
                            col = "Deterministic model")) +
    scale_x_continuous(name = "\nTime since start of policy (years)") +
    scale_y_continuous(name = "Observed annual VL incidence per 10,000 capita\n") +
    scale_color_manual(name = NULL,
                       values = c("blue", "black", "red")) +
    expand_limits(y = 0) +
    theme(legend.position = c(.95, .95),
          legend.justification = c(1, 1))
  
  compare.plot
  compare.plot + coord_cartesian(x = c(-5, 15),
                                 y = c(0, 25))
  
  # setwd(code.dir)
  # ggsave("example.pdf")
  
# # Check variation on population size
#   ggplot(data = output.stoch,
#          mapping = aes(x = time, y = N )) +
#     geom_line(mapping = aes(group = sim, col = "Stochastic (individual simulations)"),
#               alpha = 0.1) +
#     stat_summary(fun.y = mean, geom = "line",
#                  mapping = aes(col = "Stochastic (mean of repeated simulations)"),
#                  linetype = 2) +
#     geom_line(data = output.det,
#               mapping = aes(y = N,
#                             col = "Deterministic model")) +
#     scale_x_continuous(name = "\nTime (years)") +
#     scale_y_log10(name = "Total population based across time \n") +
#     scale_color_manual(name = NULL,
#                        values = c("blue", "black", "red")) +
#     expand_limits(y = 0) +
#     theme(legend.position = c(.95, .95),
#           legend.justification = c(1, 1))
  

### END OF CODE ###
  
