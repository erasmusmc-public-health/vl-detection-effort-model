# Calibrate VL transmission rate (beta) for deterministic model in pomp, and
# save values along with state distribution of population at equilibrium to use
# as initial values for stochastic simulation (if desired)

# Author: Luc Coffeng, Johanna Munoz
# Date created: 9 July 2019

# Key assumptions:
#  - fixed population size
#  - no age structure
#  - deterministic human population

# Prep
  rm(list = ls())
  
  library(pomp)
  library(ggplot2)
  library(data.table)
  library(boot)

  library(rstudioapi)  # requires RStudio
  
  code.dir <- dirname(getActiveDocumentContext()$path)
  
  setwd(file.path(code.dir, "01_Source"))
  source("pomp_VL_model.r")
  source("pomp_VL_param_values.r")
  source("pomp_VL_helper_functions.r")
  

# Avoid numerical problems by evenly dividing population over two strata
  param.values["fd"] <- 0.5
  param.values["rhoI1"] <- param.values["rhoI2"]

  
# Set simulation duration, stepsize (in years), and IRS (impact will be linearly
# interpolated over time)
  start.time <- 0
  sim.duration <- 500  # long time needed to stabilise low endemic settings
  dt.output.det <- 1 / 365
  
  
# Compile deterministic models
  time.seq.det <- seq(start.time,
                      start.time + sim.duration,
                      by = dt.output.det)
  
  model.det <- pomp(data = data.frame(dummy = NA,
                                      time = time.seq.det),
                    times = "time",
                    covar = NULL,
                    t0 = 0,
                    skeleton = vectorfield(Csnippet(vl.code.det)),
                    rinit = Csnippet(vl.det.init),
                    statenames = statenames.det,
                    paramnames = paramnames) 
  
# Predict VL treatment incidence, given beta 
  predict <- data.table(beta = seq(from = 0, to = 500, by = 10))
  
  predict[, VL_inc := {
    pars <- param.values
    pars["beta"] <- beta
    output <- trajectory(model.det, params = c(pars, init.values))
    output <- extract.output.det(x = output, time = time.seq.det)
    output[.N, VL_inc_obs] * 1e4
  }, by = .(beta)]
  
  ggplot(data = predict,
         mapping = aes(x = beta, y = VL_inc)) +
    geom_line() +
    scale_x_continuous(name = "\nbeta (transmission rate)",
                       breaks = 0:10 * 100) +
    scale_y_continuous(name = "Observed annual VL incidence per 10,000 capita",
                       breaks = 0:10 * 5) +
    expand_limits(y = 0)
  
  setwd(file.path(code.dir, "02_Output"))
  ggsave("beta vs observed VL incidence.pdf")
    
  
# Tune beta to reproduce desired VL treatment incidence at equilibrium
  VL_inc_target = c(2, 5, 10, 15, 20)
  # initialise close to solution
  fit <- predict[, .(beta.init = approx(x = VL_inc,
                                        y = beta,
                                        xout = VL_inc_target,
                                        method = "linear")$y,
                     VL_inc_target = VL_inc_target)]
  fit[, beta.init.log := log(beta.init)]
  
  sim.equilib <- function(par,         # log-beta
                          target) {    # VL treatment incidence per 10,000 capita
    pars <- param.values
    pars["beta"] <- exp(par)
    output <- trajectory(model.det, params = c(pars, init.values))
    output <- extract.output.det(x = output, time = time.seq.det)
    (output[.N, VL_inc_obs] * 1e4 - target)^2
  }
  
  fit[, c("beta", "SSE", "convergence") := {
    x <- optim(par = beta.init.log,
               fn = sim.equilib,
               method = "BFGS",
               target = VL_inc_target)
    with(x, list(exp(par), value, convergence))
  }, by = .(VL_inc_target)]
  
  fit[SSE > 0.1]  # unsuccesful fits
  

# Check to recover the VL treatment incidence after sim.duration years
  fit[, VL_inc_predict := {
    pars <- param.values
    pars["beta"] <- beta
    output <- trajectory(model.det, params = c(pars, init.values))
    output <- extract.output.det(x = output, time = time.seq.det)
    output[.N, VL_inc_obs * 1e4]
  }, by = .(VL_inc_target)]
  
  
# Check that fit at year sim.duration is not results of slow decline
  predict.trend <- lapply(1:nrow(fit), function(i) fit[i, {
    pars <- param.values
    pars["beta"] <- beta
    output <- trajectory(model.det, params = c(pars,init.values))
    output <- extract.output.det(x = output, time = time.seq.det)
    cbind(output[time %in% 0:sim.duration], VL_inc_target = VL_inc_target)
  }])
  
  predict.trend <- rbindlist(predict.trend)
  
  ggplot(data = predict.trend,
         mapping = aes(x = time, y = VL_inc_obs * 1e4,
                       color = as.factor(VL_inc_target))) +
    geom_line() +
    geom_hline(yintercept = c(2, 5, 10, 15, 20), linetype = 3) + 
    scale_color_discrete(guide = FALSE) +
    expand_limits(y = 0)
  
  setwd(file.path(code.dir, "02_Output"))
  ggsave("beta vs observed VL incidence time trend.pdf")
  
   
# Save results as if entire population was only in first stratum
  beta.fit <- merge(
    x = fit[, .(beta, VL_inc_target)],
    y = predict.trend[time == max(time)],
    by = c("VL_inc_target"))
  
  beta.fit[, I21 := I21 + I11]
  beta.fit[, I22 := I22 + I12]
  beta.fit[, I23 := I23 + I13]
  beta.fit[, VL_I21 := VL_I21 + VL_I11]
  beta.fit[, VL_I22 := VL_I22 + VL_I12]
  beta.fit[, VL_I23 := VL_I23 + VL_I13]
  beta.fit[, VL_death_2 := VL_death_2 + VL_death_1]
  
  beta.fit[, I11 := 0L]
  beta.fit[, I12 := 0L]
  beta.fit[, I13 := 0L]
  beta.fit[, VL_I11 := 0L]
  beta.fit[, VL_I12 := 0L]
  beta.fit[, VL_I13 := 0L]
  beta.fit[, VL_death_1 := 0L]
  
  beta.fit[, Reduce('+', mget(statenames.det))] - 1  # minor numerical error
  
  setwd(file.path(code.dir, "02_Output"))
  save(beta.fit, param.values,
       file = "beta_fit_pomp.RData")
  
  
  
### END OF CODE ###
  
