# Source code for deterministic and stochastic models of transmission, detection,
# and control of visceral leishmaniasis, implemented in the pomp package
# (partially observed Markov processes).

# Key assumptions:
#  - fixed population size
#  - no age structure
#  - stochastic human population
#  - deterministic fly population

# Author: Luc Coffeng
# Created: 15 July 2019
  
# Deterministic model (no age structure)
  statenames.det <- c("S", "L",
                      "I11", "I12", "I13",
                      "I21", "I22", "I23",
                      "D", "P", "R")

# Deterministic model (no age structure)

  vl.code.det <- "
  // Declare local variables
    double N;       // Total human population size
    double lambda;  // Force of infection

    N = S + L + I11 + I12 + I13 + I21 + I22 + I23 + D + P + R;
    lambda = beta * (I11 + I12 + I13 + I21 + I22 + I23 + betaP * P + betaL * L) / N;

  // System of ODEs
    DS = mu * N + 3 * muVL * (I13 + I23) + rhoR * R - (lambda + mu) * S ;
    DL = lambda * S - (rhoL + mu) * L;
    DI11 = fd * fs * rhoL * L - (rhoI1 + mu + 3 * muVL) * I11;
    DI12 = 3 * muVL * I11 - (rhoI1 + mu + 3 * muVL) * I12;
    DI13 = 3 * muVL * I12 - (rhoI1 + mu + 3 * muVL) * I13;
    DI21 = (1 - fd) * fs * rhoL * L - (rhoI2 + mu + 3 * muVL) * I21;
    DI22 = 3 * muVL * I21 - (rhoI2 + mu + 3 * muVL) * I22;
    DI23 = 3 * muVL * I22 - (rhoI2 + mu + 3 * muVL) * I23;
    DD = rhoI1 * (I11 + I12 + I13) + rhoI2 * (I21 + I22 + I23) - (rhoD + mu) * D;
    DP = fp * rhoD * D - (rhoP + mu) * P;
    DR = (1 - fs) * rhoL * L + (1 - fp) * rhoD * D + rhoP * P - (rhoR + mu) * R;
  "
  

  vl.det.init <- "
    double N_0;  // local variable for sum of initial human compartments sizes
  
    N_0 = S_0 + L_0
        + I11_0 + I12_0 + I13_0
        + I21_0 + I22_0 + I23_0
        + D_0 + P_0 + R_0;
  
    S = S_0/N_0;
    L = L_0/N_0;
    I11 = I11_0/N_0;
    I12 = I12_0/N_0;
    I13 = I13_0/N_0;
    I21 = I21_0/N_0;
    I22 = I22_0/N_0;
    I23 = I23_0/N_0;
    D = D_0/N_0;
    P = P_0/N_0;
    R = R_0/N_0;
  
  "



  
# Stochastic compartmental model (no age structure, fixed population size)
  counternames.stoch <- c("VL_I11","VL_I12","VL_I13",
                          "VL_I21","VL_I22","VL_I23",
                          "VL_inc","VL_death_1","VL_death_2")
  statenames.stoch <- c(statenames.det, counternames.stoch)
  
  vl.code.stoch <- "
  // Declare local variables
    double N;         // Total human population size
    double lambda;    // Force of infection
    double rate[31];  // Rates (31 = length, indexing starts at 0)
    double dN[31];    // Derivatives (discrete number of persons that move between compartments)
    
  // Define local variables 
    N = S + L + I11 + I12 + I13 + I21 + I22 + I23 + D + P + R;
    lambda = beta * (I11 + I12 + I13 + I21 + I22 + I23 + betaP * P + betaL * L) / N;
  
  // Transitions of discrete numbers of humans exiting each compartment
  // N.B. for the reulermultinom() function, exit rates for a given compartment
  // must be next to eachother in the rate[] vector
  
  // S (susceptibles)
  // DS = ...  - (lambda + mu) * S ;
    rate[0] = lambda;                   // infection rate
    rate[1] = mu;                       // death
    reulermultinom(2, S, &rate[0], dt, &dN[0]);

  // L (latent infection)
  // DL = ... - (rhoL + mu) * L;
    rate[2] = fd * fs * rhoL;           // progression to symptomatic infection phase with high probability of detection
    rate[3] = (1 - fd) * fs * rhoL;     // progression to symptomatic infection phase with low probability of detection
    rate[4] = (1 - fs) * rhoL;          // spontaneous recovery
    rate[5] = mu;                       // death due to other causes
    reulermultinom(4, L, &rate[2], dt, &dN[2]);
    
  // I11 (Symptomatic infection with high probability of being detected and treated  - Erlang compartment 1)
  // DI11 = ... - (rhoI1 + mu + 3 * muVL) * I11;
    rate[6] = rhoI1;                    // progression to  dormant stage
    rate[7] = mu;                       // death due to other causes
    rate[8] = 3 * muVL;                 // death due to VL
    reulermultinom(3, I11, &rate[6], dt, &dN[6]);

  // I12 (Symptomatic infection with high probability of being detected and treated  - Erlang compartment 2)
  //  DI12 = ... - (rhoI1 + mu + 3 * muVL) * I12;
    rate[9] = rhoI1;                    // progression to  dormant stage
    rate[10] = mu;                      // death due to other causes
    rate[11] = 3 * muVL;                // death due to VL
    reulermultinom(3, I12, &rate[9], dt, &dN[9]);

  // I13 (Symptomatic infection with high probability of being detected and treated  - Erlang compartment 3)
  // DI13 = ... - (rhoI1 + mu + 3 * muVL) * I13;
    rate[12] = rhoI1;                   // progression to  dormant stage
    rate[13] = mu;                      // death due to other causes
    rate[14] = 3 * muVL;                // death due to VL
    reulermultinom(3, I13, &rate[12], dt, &dN[12]);

  // I21 (Symptomatic infection with low probability of being detected and treated  - Erlang compartment 1)
  // DI21 = ... - (rhoI2 + mu + 3 * muVL) * I21;
    rate[15] = rhoI2;                   // progression to  dormant stage
    rate[16] = mu;                      // death due to other causes
    rate[17] = 3 * muVL;                // death due to VL
    reulermultinom(3, I21, &rate[15], dt, &dN[15]);


  // I22 (Symptomatic infection with low probability of being detected and treated  - Erlang compartment 2)
  // DI22 =  ... - (rhoI2 + mu + 3 * muVL) * I22;
    rate[18] = rhoI2;                  // progression to  dormant stage
    rate[19] = mu;                     // death due to other causes
    rate[20] = 3 * muVL;               // death due to VL
    reulermultinom(3, I22, &rate[18], dt, &dN[18]);

  // I23 (Symptomatic infection with low probability of being detected and treated  - Erlang compartment 3)
  // DI23 =  ... - (rhoI2 + mu + 3 * muVL) * I23;
    rate[21] = rhoI2;                 // progression to  dormant stage
    rate[22] = mu;                    // death due to other causes
    rate[23] = 3 * muVL;              // death due to VL
    reulermultinom(3, I23, &rate[21], dt, &dN[21]);

  // D (Dormant stage)
  // DD = ... - (rhoD + mu) * D;
    rate[24] = (1 - fp) * rhoD;       // full recovery
    rate[25] = fp * rhoD;             // PKDL
    rate[26] = mu;                    // death due to other causes
    reulermultinom(3, D, &rate[24], dt, &dN[24]);

  // P (PKDL)
  // DP = ... - (rhoP + mu ) * P;
    rate[27] = rhoP;                  // full recovery
    rate[28] = mu;                    // death due to other causes
    reulermultinom(2, P, &rate[27], dt, &dN[27]);

  // R (Recovered)
  // DR = ... - (rhoR + mu ) * R;
    rate[29] = rhoR;                  // seroreversion rate
    rate[30] = mu;                    // death due to other causes
    reulermultinom(2, R, &rate[29], dt, &dN[29]);

  // System of ODEs
    //DS = mu * N + 3 * muVL * (I13 + I23) + rhoR * R - (lambda + mu) * S ;
    S += dN[5] + dN[7] + dN[10] + dN[13] + dN[16] + dN[19] + dN[22] + dN[26] + dN[28] + dN[30] + dN[14] + dN[23] + dN[29] - dN[0];  // dN[1] terms cancel out

    //DL = lambda * S - (rhoL + mu) * L;
    L += dN[0] - dN[2] - dN[3] - dN[4] -dN[5];

    //DI11 = fd * fs * rhoL * L - (rhoI1 + mu + 3 * muVL) * I11;
    I11 += dN[2] - dN[6] - dN[7] - dN[8];

    //DI12 = 3 * muVL * I11 - (rhoI1 + mu + 3 * muVL) * I12;
    I12 += dN[8] - dN[9] - dN[10] - dN[11];

    //DI13 = 3 * muVL * I12 - (rhoI1 + mu + 3 * muVL) * I13;
    I13 += dN[11] - dN[12] - dN[13] - dN[14];

    //DI21 = (1 - fd) * fs * rhoL * L - (rhoI2 + mu + 3 * muVL) * I21;
    I21 += dN[3] - dN[15] - dN[16] - dN[17];

    //DI22 = 3 * muVL * I21 - (rhoI2 + mu + 3 * muVL) * I22;
    I22 += dN[17] - dN[18] - dN[19] - dN[20];

    //DI23 = 3 * muVL * I22 - (rhoI2 + mu + 3 * muVL) * I23;
    I23 += dN[20] - dN[21] - dN[22] - dN[23];

    //DD = rhoI1 * (I11 + I12 + I13) + rhoI2 * (I21 + I22 + I23) - (rhoD + mu) * D;
    D += dN[6] + dN[9] + dN[12] + dN[15] + dN[18] + dN[21] - dN[24] - dN[25] - dN[26];

    //DP = fp * rhoD * D - (rhoP + mu) * P;
    P += dN[25] - dN[27] - dN[28];

    //DR = (1 - fs) * rhoL * L + (1 - fp) * rhoD * D + rhoP * P - (rhoR + mu) * R;
    R += dN[4] + dN[24] + dN[27] - dN[29] - dN[30];


    // Accumulators to record numbers of incident cases per time window
    VL_I11 += dN[6];      // Record per Erlang stage to derive average durations
    VL_I12 += dN[9];      // since start of symptoms under different scenarios
    VL_I13 += dN[12];     // for detection effort.
    VL_I21 += dN[15];
    VL_I22 += dN[18];
    VL_I23 += dN[21];
    VL_inc += dN[2] + dN[3];     // True incidence of visceral leishmaniasis
    VL_death_1 += dN[14];        // Deaths due to untreated visceral leishmaniasis on group with high probability of being detected and treated.
    VL_death_2 += dN[23];        // Deaths due to untreated visceral leishmaniasis on group with low probability of being detected and treated.
  "
    
  vl.stoch.init <- "
    S = S_0;
    L = L_0;
    I11 = I11_0;
    I12 = I12_0;
    I13 = I13_0;
    I21 = I21_0;
    I22 = I22_0;
    I23 = I23_0;
    D = D_0;
    P = P_0;
    R = R_0;
    VL_I11 = 0;          // Incidence detected for each group of risk of
    VL_I12 = 0;          // detection and Erlang compartment
    VL_I13 = 0;
    VL_I21 = 0;
    VL_I22 = 0;
    VL_I23 = 0;
    VL_inc = 0;        // True incidence of visceral leishmaniasis
    VL_death_1 = 0;     // Deaths due to untreated visceral leishmaniasis on group with high probability of being detected and treated.
    VL_death_2 = 0;
  "
    
  
### END OF CODE ###
  
