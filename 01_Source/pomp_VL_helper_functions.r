# Function to extract and prep output from deterministic VL model in pomp
#   x = object produced by pomp's trajectory() function
#   time = vector of time points (must match dimensions of x)
#   pars = vector of parameter values used in simulations
#   covars = NULL or covariate table used in the simulations
  extract.output.det <- function(x,
                                 time,
                                 pars = param.values,
                                 covars = NULL) {
    
    
    if(dim(x)[3] != length(time)) stop("Mismatch in dimensions of x and time")
    
    x <- as.data.table(t(x[dimnames(x)[[1]], , ]))
    
    state.names <- names(x)[1:11]
    
    # Add columns
    x[, time := time]  
    x[, N := Reduce("+", mget(state.names))]
    
    if(!is.null(covars)) {
    # Function to interpolate variables in the covars table over time
      interpolation <- function(y, covars) {
        approx(x = t(covars@times),
               y = y,
               xout = time,
               rule = 1,
               method = ifelse(covars@order == 0, "constant", "linear"))
      }
      
      inter.list <- apply(data.frame(t(covars@table)),
                          2,
                          interpolation,
                          covars = covars)
      inter.y <- lapply(inter.list, `[[`, 2)
      y <- do.call(cbind, inter.y)
      
      x <- cbind(x, y)
      
    # Calculate incidence of observed VL
      x[, VL_I11 := rhoI1 * I11]
      x[, VL_I12 := rhoI1 * I12]
      x[, VL_I13 := rhoI1 * I13]
      x[, VL_I21 := rhoI2 * I21]
      x[, VL_I22 := rhoI2 * I22]
      x[, VL_I23 := rhoI2 * I23]
      
    # Calculate duration of Erlang stages (corrected for policy)
      x[, dur.erlang.1 := with(as.list(pars), 1 / (rhoI1 + mu + muVL*3))]
      x[, dur.erlang.2 := with(as.list(pars), 1 / (rhoI2 + mu + muVL*3))]
      x[, colnames(y) := NULL]
      
    } else {
      if(is.na(pars["rhoI1"]) | is.na(pars["rhoI2"]))
        stop("Check parameter list, rhoI1 or rhoI2 are not specified")
      
      x[, VL_I11 := with(as.list(pars), rhoI1 * I11 )]
      x[, VL_I12 := with(as.list(pars), rhoI1 * I12 )]
      x[, VL_I13 := with(as.list(pars), rhoI1 * I13 )]
      x[, VL_I21 := with(as.list(pars), rhoI2 * I21 )]
      x[, VL_I22 := with(as.list(pars), rhoI2 * I22 )]
      x[, VL_I23 := with(as.list(pars), rhoI2 * I23 )]
      
    # Calculate duration of Erlang stages
      x[, dur.erlang.1 := with(as.list(pars), 1 / (rhoI1 + mu + muVL*3))]
      x[, dur.erlang.2 := with(as.list(pars), 1 / (rhoI2 + mu + muVL*3))]
      
    }
    
  # Calculate total VL incidence (observed and true)
    x[, VL_inc_obs := VL_I11 + VL_I12 + VL_I13 + VL_I21 + VL_I22 + VL_I23]
    x[, VL_inc := with(as.list(pars), fs * rhoL * L)]
    
  # Calculate mortality rate due to VL
    x[, VL_death_1 := with(as.list(pars), 3 * muVL * I13)]
    x[, VL_death_2 := with(as.list(pars), 3 * muVL * I23)]
    
  # Calculate dwelling times for cases of VL (duration of symptoms until
  # diagnosis) and deaths due to VL (duration of symptoms until death)
    x[, dur.symptom.obs :=
        (dur.erlang.1 * cbind(VL_I11, VL_I12, VL_I13) %*% 1:3 +
           dur.erlang.2 * cbind(VL_I21, VL_I22, VL_I23) %*% 1:3) /
        VL_inc_obs * 365]
    x[, dur.symptom.death :=
        (dur.erlang.1 * 3 * VL_death_1 + dur.erlang.2 * 3 * VL_death_2) / 
        (VL_death_1 + VL_death_2) * 365]
    
  # Clean up
    x[, dur.erlang.1 := NULL]
    x[, dur.erlang.2 := NULL]
    
  # Return result
    x
    
  }
  

# Function to run stochastic model and returns a single data.table containing
# (repeated) simulation output
#   time.output = integer vector indicating at what time point output is
#                 generated, note that counter variables (incident cases and
#                 deaths) will be aggregated over the entire period between
#                 any two time points
#   time.step = Euler step size used within simulation
#   t0 = time point at which simulation starts
#   pars = vector of parameter values that are fixed during each simulation and
#          between repeated simulations
#   init = named vector with initial values or matrix with columns of initial
#          values for multiple simulations
#   covar = data.frame or data.table of time-dependent parameter values
  run.stoch <- function(time.output = seq(0, 50, by = 1),
                        time.step = 1 / (2 * 365),
                        t0 = 0,
                        pars = param.values,
                        init = NULL,
                        covars = NULL,
                        nsim = 1,
                        seed = 1914679908L) {
    
  # Muffle a specific warning from the pomp package about 'rmeasure' not being
  # defined
    withCallingHandlers({
      if(is.vector(init)) {
        parameters <- c(pars, init)
        param.names <- names(parameters)
      }
      if(is.matrix(init)) {
        parameters <- rbind(matrix(pars,
                                   nrow = length(pars),
                                   ncol = ncol(init),
                                   dimnames = list(names(pars), NULL)),
                            init)
        param.names <- rownames(parameters)
      }
      
    # Run model
      pomp.object <- pomp(
        data = data.frame(time = time.output),
        times = "time",
        t0 = t0,
        rinit = Csnippet(vl.stoch.init),
        rprocess = euler(step.fun = Csnippet(vl.code.stoch),
                         delta.t = time.step),
        statenames = statenames.stoch,
        paramnames = param.names,
        accumvars = counternames.stoch,
        covar = covars)
      
      output <- simulate(
        object = pomp.object,
        params = parameters,
        nsim = nsim,
        seed = seed,
        format = "data.frame")
      
      x <- as.data.table(output)
      state.names <- names(x)[3:13]
      x[, N := Reduce("+", mget(state.names))]
      x[, VL_inc_obs := VL_I11 + VL_I12 + VL_I13 + VL_I21 + VL_I22 + VL_I23]
      
      setnames(x, old = ".id", new = "sim")
      x
      
    }, warning = function(w) {
      if(any(grepl("'rmeasure' unspecified: NAs generated", w)))
        invokeRestart( "muffleWarning" )
    })
    
  }
  
  
# Function to analytically calculate undetected dwell time for a population of
# undetected symptomatic cases subject to some background mortality rate, excess
# mortality due to VL, the detection rate, and the shape of Erlang distribution
# for time until death. The function returns a vector of values, which if summed
# up represent the average undetected dwell time. The individual vector elements
# represent the relative size of the population being detection in the different
# Erlang stages.
  calc.undetect.dwell <- function(mu, muVL, rho, shape = 3) {
    
  # Calculate dwell time per compartment
    dwell.compartment <- 1 / (mu + shape * muVL + rho)
    
  # Calculate proportions making it to specific exits
    prop <- matrix(NA,
                   nrow = shape,
                   ncol = 3,
                   dimnames = list(paste0("Erlang_", 1:shape),
                                   c("Detected", "Progressed", "Death due to other causes")))
    par.vector <- c(rho, shape * muVL, mu)
    
    prop[1, ] <- par.vector / sum(par.vector)
    for(i in 2:shape) prop[i, ] <- par.vector / sum(par.vector) * prop[(i - 1), 2]
    
    # sum(prop) - sum(prop[1:(shape - 1), "Progressed"])  # should be 1.0
    
    prop[, "Detected"] * 1:shape * dwell.compartment / sum(prop[, "Detected"])
    
  }

  
# Function to numerically calibrate detection rate, given some desired undetected
# dwell time (inverse function of calc.detect.dwell()). N.B. is sensitive to
# initial values
  calibrate.rho <- function(target.dur,  # duration in days!
                            rho_init = param.values["rhoI1"] * 2,
                            mu = param.values["mu"],
                            muVL = param.values["muVL"],
                            shape = 3) {
    
    temp <- optim(par = log(rho_init),
                  fn = function(x, target) {
                    (sum(calc.undetect.dwell(mu = mu,
                                             muVL = muVL,
                                             rho = exp(x),
                                             shape = shape)) * 365 - target)^2
                  }, method = "BFGS", target = target.dur)
    
    # if(temp$value > 1) stop("Non-convergence; provide better inits")
    # temp$par <- exp(temp$par)
    # temp
    if(temp$value > 1) return(NA)
    exp(temp$par)
    
  }
  

### END OF CODE ###
  
