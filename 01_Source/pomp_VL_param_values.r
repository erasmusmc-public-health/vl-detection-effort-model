# Set parameter values (all rates defined per year) and initial states
  param.values <- c(beta = 83.86715,        # Overall transmission rate, incorporating sandfly density, sandfly biting rate, and transmission probability from fly to human
                    #    VL_inc_target      beta
                    # 1:             2  83.86715
                    # 2:             5  93.86728
                    # 3:            10 117.11511
                    # 4:            15 155.66924
                    # 5:            20 232.06467
                    betaP = 0.9,            # Infectivity of post-kala-azar dermal leishmaniasis relative to visceral leishmaniasis
                    betaL = 0,              # Infectivity of latent infection relative to visceral leishmaniasis
                    
                    mu = 1/68,              # Background mortality rate per capita
                    muVL = 365/189,         # Excess mortality rate due to untreated visceral leishmaniasis.
                    
                    fd = 0.0,               # Proportion of humans in whom symptomatic infection is more easily detected
                    fs = 0.03,              # Proportion of infections that progress to visceral leishmaniasis
                    fp = 0.05,              # Proportion of visceral leishmaniasis cases that develop post-kala-azar dermal leishmaniasis
                    
                    rhoL = 365/150,         # 1 / Average duration of latent infection.
                    rhoI1 = 365/243,        # 1 / Average duration until detection and treatment of visceral leishmaniasis in group with higher or lower probability of being detected and treated.
                    rhoI2 = 365/243,        # 1 / Average duration until detection and treatment of visceral leishmaniasis in group with default probability of being detected and treated.
                    
                    rhoD = 12/21,           # 1 / Average duration of the dormant stage.
                    rhoP = 1/5,             # 1 / Average duration of post-kala-azar dermal leishmaniasis.
                    rhoR = 1/5              # 1 / Average duration of the recovered (immune) stage.
  )
  

# Initial distribution of human population over compartments
  init.values = c(S_0 = .99, L_0 = 0,
                  I11_0 = 0, I12_0 = 0, I13_0 = 0,
                  I21_0 = 0, I22_0 = 0, I23_0 = 0,
                  D_0 = 0, P_0 = .01, R_0 = 0)

  paramnames <- c(names(param.values),
                  names(init.values))
  
  
### END OF CODE ###
  
